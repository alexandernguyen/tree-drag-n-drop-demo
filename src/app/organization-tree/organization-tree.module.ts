import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { OrganizationTreeComponent } from './organization-tree.component';
import { Router, RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        OrganizationTreeComponent
    ],
    imports: [
        RouterModule.forChild([{
            path: '', component: OrganizationTreeComponent
        }]),
        SharedModule,
    ]
})
export class OrganizationTreeModule { }
