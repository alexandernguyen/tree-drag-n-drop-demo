import { Component, OnInit, OnDestroy } from '@angular/core';
import { GetTreeInfoService } from 'app/services/get-tree-info.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TreeInfo, DataTree } from 'app/shared/models/tree-info.model';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-organization-tree',
  templateUrl: './organization-tree.component.html',
  styleUrls: ['./organization-tree.component.scss']
})
export class OrganizationTreeComponent implements OnInit, OnDestroy {
  public title = '';
  public dataTree: Array<DataTree> = [];

  private destroyed = new ReplaySubject<any>();

  constructor(private readonly treeService: GetTreeInfoService) {
  }

  ngOnInit() {
    this.treeService.getTreeInfo()
      .pipe(takeUntil(this.destroyed))
      .subscribe((treeInfo: TreeInfo) => {
        this.title = treeInfo.class;
        treeInfo.data.forEach(item => {
          if (!item.parent) {
            const parent = { parent: item, childs: [] };
            this.dataTree.push(parent);
          } else {
            const parent = this.dataTree.find((it) => it.parent.id === item.parent);
            parent.childs.push(item);
          }
        });
        this.dataTree.sort((a: any, b: any) => (+a.parent.id > +b.parent.id) ? 1 : ((+b.parent.id > +a.parent.id) ? -1 : 0));
      });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }
}
