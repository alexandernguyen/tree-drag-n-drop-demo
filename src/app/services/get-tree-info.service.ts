import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetTreeInfoService {

   constructor(private httpClient: HttpClient) {
   }

  public getTreeInfo() {
    return this.httpClient.get('assets/tree-info.json');
  }
}
