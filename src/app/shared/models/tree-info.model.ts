export interface TreeInfo {
    class: string;
    data: TreeElement[];
}

export interface DataTree {
    parent: TreeElement;
    childs: TreeElement[];
}

export interface TreeElement {
    id: number;
    title: string;
    parent?: number;
}
