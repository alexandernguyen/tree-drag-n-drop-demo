import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { MatExpansionModule, } from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        MatExpansionModule
    ],
    exports: [
        CommonModule,
        DragDropModule,
        MatExpansionModule
    ]
})

export class SharedModule { }
